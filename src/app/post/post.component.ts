import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoveIts: number;
  @Input() postCreated_at: Date;

  constructor() { }

  lastUpdate = new Date();

  ngOnInit() {
  }

  onYes() {
    console.log( 'J\'aime! = +1 ; Compteur de l\'article ' + this.postTitle + ' = ' + this.postLoveIts );
    this.postLoveIts = this.postLoveIts + 1;
    // @ts-ignore
    console.log(this.postCreated_at = 'Mis à jour à ' + new Date());
}
  onNo() {
    console.log('Je n\'aime Pas! = -1 ; Compteur de l\'article ' + this.postTitle + ' = ' + this.postLoveIts);
    this.postLoveIts = this.postLoveIts - 1;
    // @ts-ignore
    console.log(this.postCreated_at = 'Mis à jour à ' + new Date());
  }

  getColorBg() {
    if (this.postLoveIts < 0) {
      return '#ffc9c9';
    } else if (this.postLoveIts > 0 ) {
      return '#cbffc9';
    }
  }

  getColorFont() {
    if(this.postLoveIts < 0) {
      return '#661010';
    } else if (this.postLoveIts > 0 ) {
      return '#116624';
    }
  }

}
